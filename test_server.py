import quart
import asyncio
import json

app = quart.Quart(__name__)

@app.websocket('/api/by-path/<path:path>/by-interface/<string:interface>/signals/<string:member>')
async def handle_signal_websocket(path: str, interface: str, member: str):
    await quart.websocket.accept()
    state = 0
    while True:
        await asyncio.sleep(5)
        await quart.websocket.send(json.dumps({
            'member': 'ItemStateChanged',
            'args': {
                'item': 'button_left',
                'state': state
            }
        }))
        state = (state + 1) % 2

@app.get('/items')
async def handle_items():
    data = [
        {
            'id': 'button_left',
            'position': {
                'x': 0.1,
                'y': 0.1
            },
            'direction': 'input'
        },
        {
            'id': 'button_right',
            'position': {
                'x': 0.9,
                'y': 0.1
            },
            'direction': 'input'
        },
        {
            'id': 'flipper_left',
            'position': {
                'x': 0.33,
                'y': 0.28
            },
            'direction': 'output'
        }
    ]
    return quart.Response(
        response = json.dumps(data),
        headers={
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
        }
    )

